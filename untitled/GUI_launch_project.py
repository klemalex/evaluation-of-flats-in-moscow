from tkinter import *
import tkinter.ttk as ttk
from main import main
from tools import area_values_names, area_values_numbers
from predictions import prediction


class AbstractWindow:
    def __init__(self, rt):
        self.rt = rt
        self.rt.resizable(False, False)
        ttk.Style().theme_use('xpnative')
        self.std_font = ("Times New Roman", 14)
        self.without_train = False


class SetParametersWindow(AbstractWindow):
    def __init__(self, rt, title='Ввод параметров сети', geometry='700x900+500+50',
                 image='neural.png',text_label="Выберите параметры нейронной сети"):
        AbstractWindow.__init__(self, rt)

        # root settings
        self.rt.title(title)
        self.rt.geometry(geometry)
        self.photo_neural = PhotoImage(file=image)
        self.label = ttk.Label(rt, image=self.photo_neural)
        self.label.pack()
        ttk.Label(self.rt, text=text_label, font=("Times New Roman", 16, 'bold')).place(x=120, y=210)

    def train_callback(self):
        self.rt.destroy()

    def without_train_callback(self):
        self.without_train = True
        self.rt.destroy()

    def set_number_of_hidden_neurons(self,label_text="Количество нейронов в скрытом слое:"):
        self.hidden = IntVar()
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=270)
        self.number_chosen = ttk.Combobox(self.rt, textvariable=self.hidden, width=12, justify="center")
        self.number_chosen.place(x=500, y=275)
        self.number_chosen['values'] = (
        2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
        32, 33, 34, 50, 70, 100, 150, 200)
        self.number_chosen.current(15)

    def set_learning_rate(self, label_text="Скорость обучения:"):
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=310)
        self.l_rate = DoubleVar(self.rt, value='0.05')
        Spinbox(self.rt, from_=0.001, to=1.0, increment=0.001, width=14, textvariable=self.l_rate, justify="center").place(
            x=500, y=315)

    def momentum(self, label_text="Момент:"):
        # momentum
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=350)
        self.moment = DoubleVar(self.rt, value='0.9')
        Spinbox(self.rt, from_=0, to=1.0, increment=0.001, width=14, textvariable=self.moment, justify="center").place(x=500,y=355)

    def batch(self, label_text="Размер пакетов:"):
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=390)
        self.batch = DoubleVar(self.rt, value='0.9')
        self.numberChosen = ttk.Combobox(self.rt, width=12, textvariable=self.batch, justify="center")
        self.numberChosen.place(x=500, y=395)
        self.numberChosen['values'] = (16, 32, 64, 128, 256, 512, 1024, 2048)
        self.numberChosen.current(4)

    def low_weight(self, label_text="Минимальный начальный вес:"):
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=450)
        self.l_weight = DoubleVar(self.rt, value='-0.2')
        Spinbox(self.rt, from_=-1.00, to=1.00, increment=0.001, width=14, textvariable=self.l_weight, justify="center").place(
            x=500, y=455)

    def high_weight(self, label_text="Максимальный начальный вес:"):
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=490)
        self.h_weight = DoubleVar(self.rt, value='0.2')
        Spinbox(self.rt, from_=-1.00, to=1.00, increment=0.001, width=14, textvariable=self.h_weight, justify="center").place(
            x=500, y=495)

    def bias(self, label_text="Начальное смещение:"):
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=530)
        self.bias = DoubleVar(self.rt, value='0.05')
        Spinbox(self.rt, from_=-1.00, to=1.00, increment=0.001, width=14, textvariable=self.bias, justify="center").place(x=500,
                                                                                                                  y=535)

    def max_iterations(self, label_text="Максимальное количество эпох:"):
        self.max_it = IntVar(self.rt, value='700')
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=590)
        ttk.Entry(self.rt, width=15, textvariable=self.max_it, justify="center").place(x=500, y=595)

    def error_limit(self, label_text="Минимальная ошибка обучения:"):
        self.err_limit = DoubleVar(root, value='0.001')
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=630)
        Spinbox(self.rt, from_=0.05, to=0.3, increment=0.001, width=14, textvariable=self.err_limit, justify="center").place(
            x=500, y=635)

    def stop(self, label_text="Ранняя остановка:"):
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=670)
        self.stop_num = IntVar(self.rt)
        self.number_chosen2 = ttk.Combobox(self.rt, width=12, textvariable=self.stop_num, justify="center")
        self.number_chosen2.place(x=500, y=675)
        self.number_chosen2['values'] = (2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 50, 100)
        self.number_chosen2.current(15)

    def hidden_act_func(self, label_text="Функция активации скрытого слоя:"):
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=730)
        self.var = IntVar()
        ttk.Radiobutton(self.rt, text='Sigmoid', variable=self.var, value=1).place(x=450, y=730)
        ttk.Radiobutton(self.rt, text='Tanh', variable=self.var, value=2).place(x=534, y=730)
        ttk.Radiobutton(self.rt, text='Softsign', variable=self.var, value=3).place(x=590, y=730)
        self.var.set(1)

    def output_act_func(self,label_text="Функция активации выходного слоя:"):
        Label(self.rt, text=label_text, font=self.std_font).place(x=20, y=770)
        self.var2 = IntVar()
        ttk.Radiobutton(self.rt, text='Tanh', variable=self.var2, value=1).place(x=450, y=770)
        ttk.Radiobutton(self.rt, text='ReLu', variable=self.var2, value=2).place(x=534, y=770)
        ttk.Radiobutton(self.rt, text='Softplus', variable=self.var2, value=3).place(x=590, y=770)
        self.var2.set(1)

    def buttons(self):
        ttk.Button(self.rt, text='Обучить', command=self.train_callback).place(x=450, y=830)
        ttk.Button(self.rt, text='Оценить c наилучшей конфигурацией', command=self.without_train_callback).place(x=150, y=830)


class PrintNetworkResultWindow(AbstractWindow):

    def __init__(self, rt, epoch, train_error, valid_error, test_error,
                 r_squere_train, r_squere_valid, r_squere_test, title=' Результат обучения сети',
                 geometry='650x300+500+50'):

        AbstractWindow.__init__(self, rt)
        self.rt.title(title)
        self.rt.geometry(geometry)
        self.std_font = ("Times New Roman", 12)
        self.epoch = epoch
        self.train_error = train_error
        self.valid_error = valid_error
        self.test_error = test_error
        self.r_squere_train = r_squere_train
        self.r_squere_valid = r_squere_valid
        self.r_squere_test = r_squere_test

    def evaluate_callback(self):
        self.rt.destroy()

    def label(self, my_text, string, xcoord, ycoord ):
        ttk.Label(text=my_text + "  " + string, font=self.std_font).place(x=xcoord, y=ycoord)

    def output_info(self):
        ttk.Label(text="Результат обучения сети ", font=("Times New Roman", 13, 'bold')).place(x=190, y=10)
        text_options = [("Количество пройденных эпох:",str(self.epoch),180,50),
                        ("Ошибка обучения:", str(round(self.train_error, 4)), 20, 130),
                        ("Ошибка валидации:", str(round(self.valid_error, 4)), 20, 170),
                        ("Ошибка тестирования:", str(round(self.test_error, 4)), 20, 210),
                        ("Коэффициент детерминации:", str(round(self.r_squere_train, 4)), 300, 130),
                        ("Коэффициент детерминации:", str(round(self.r_squere_valid, 4)), 300, 170),
                        ("Коэффициент детерминации:", str(round(self.r_squere_test, 4)), 300, 210),
                        ]
        for option in text_options:
            PrintNetworkResultWindow.label(self, *option)

    def evaluate_button(self):
        ttk.Button(self.rt, text='Оценить', command=self.evaluate_callback).place(x=280, y=250)


class GetFlatWindowResults(AbstractWindow):
    def __init__(self, rt, geometry='800x800+500+50', title=' Ввод параметров квартиры',
                 flat_image='flat.png'):

        AbstractWindow.__init__(self, rt)

        # root settings
        self.pricelabel = Label(self.rt, text=int('0'), font=("Times New Roman", 14, "bold"))
        self.pricelabel.place(x=140, y=730)
        self.rt.geometry(geometry)
        self.rt.title(title)
        self.photo_flat = PhotoImage(file=flat_image)
        self.photo_label = ttk.Label(self.rt, image=self.photo_flat)
        self.photo_label.pack()
        self.flat_parameters = {}
        ttk.Label(root3, text='Введите параметры квартиры', font=("Times New Roman", 16, 'bold')).place(x=190, y=225)

    def flat_callback(self, num_parameter, combobox):
        self.flat_parameters.update({num_parameter: combobox.current()})

    def prediction_callback(self):
        result = prediction(self.flat_parameters.get('num_metro') + 1,
                            (self.rooms.get() - (6 + 1) / 2) / ((6 - 1) / 2),
                            area_values_numbers[self.flat_parameters.get('num_area')],
                            self.penthouse.get() - 1,
                            (self.all_squere.get() - (500 + 19.9) / 2) / ((500 - 19.9) / 2),
                            (self.live_squere.get() - (283 + 8.8) / 2) / ((283 - 8.8) / 2),
                            (self.kitchen_squere.get() - (50 + 3.0) / 2) / ((50 - 3.0) / 2),
                            self.flat_parameters.get('num_floor') + 1,
                            self.flat_parameters.get('home_type') + 1,
                            self.parking.get() - 1,
                            self.flat_parameters.get('num_repairs') + 1,
                            self.flat_parameters.get('num_window') + 1,
                            (self.ceiling.get() - (5.00 + 2.48) / 2) / ((5.00 - 2.48) / 2),
                            (self.bathroom.get() - (5 + 1) / 2) / ((5 - 1) / 2),
                            (self.balcony.get() - (5 + 1) / 2) / ((5 - 1) / 2),
                            self.guard.get() - 1,
                            self.sale_t.get() - 1,
                            self.without_train)
        # update result on the window
        self.pricelabel.configure(text=int(result))

    def set_area(self):
        area = StringVar()
        Label(self.rt, text="Район:", font=self.std_font).place(x=20, y=280)
        area_combobox = ttk.Combobox(self.rt, state="readonly", values=area_values_names, textvariable=area, width=25,
                                     justify="center")
        area.trace('w', lambda *args, v=area: self.flat_callback('num_area', area_combobox))
        area_combobox.place(x=100, y=280)

    def set_distance_to_metro(self):
        distance_to_metro = StringVar()
        Label(self.rt, text="Расстояние до метро, м:", font=self.std_font).place(x=340, y=280)
        metro_combobox = ttk.Combobox(self.rt, state="readonly", values=(
            "< 300", "300-600", "600-900", "900-1200", "1200-1500", "1500-2000", "2000-4000", "4000-7000", "7000-10000",
            ">10000"), textvariable=distance_to_metro, width=19, justify="center")

        distance_to_metro.trace('w', lambda *args, v=distance_to_metro: self.flat_callback('num_metro',metro_combobox))
        metro_combobox.place(x=590, y=280)

    def set_squere(self):
        self.all_squere = DoubleVar(self.rt, value='70')
        self.live_squere = DoubleVar(self.rt, value='40')
        self.kitchen_squere = DoubleVar(self.rt, value='15')
        Label(self.rt, text="Площадь общая / жилая / кухни, м^2:", font=self.std_font).place(x=20, y=380)
        ttk.Entry(self.rt, width=5, textvariable=self.all_squere, justify="center").place(x=478, y=380)
        ttk.Entry(self.rt, width=5, textvariable=self.live_squere, justify="center").place(x=600, y=380)
        ttk.Entry(self.rt, width=5, textvariable=self.kitchen_squere, justify="center").place(x=720, y=380)

    def set_rooms_balcons_bathrooms(self):
        self.rooms = IntVar(self.rt, value='2')
        self.balcony = IntVar(self.rt, value='1')
        self.bathroom = IntVar(self.rt, value='1')
        Label(self.rt, text="Количество комнат / балконов / санузлов:", font=self.std_font).place(x=20, y=440)
        ttk.Entry(self.rt, width=5, textvariable=self.rooms, justify="center").place(x=478, y=440)
        ttk.Entry(self.rt, width=5, textvariable=self.balcony, justify="center").place(x=600, y=440)
        ttk.Entry(self.rt, width=5, textvariable=self.bathroom, justify="center").place(x=720, y=440)

    def set_windows(self):
        window = StringVar()
        Label(self.rt, text="Окна:", font=self.std_font).place(x=20, y=500)
        window_combobox = ttk.Combobox(self.rt, state="readonly",
                                       values=("На улицу", "Во двор", "На улицу и двор"),
                                       textvariable=window, width=16, justify="center")

        window.trace('w', lambda *args, v=window: self.flat_callback('num_window', window_combobox))
        window_combobox.place(x=100, y=500)

    def set_seilings(self):
        self.ceiling = DoubleVar(self.rt, value='2.5')
        Label(self.rt, text="Высота потолков, м:", font=self.std_font).place(x=265, y=500)
        ttk.Entry(self.rt, width=5, textvariable=self.ceiling, justify="center").place(x=478, y=500)

    def set_repairs(self):
        repairs = StringVar()

        Label(self.rt, text="Ремонт:", font=self.std_font).place(x=540, y=500)
        repairs_combobox = ttk.Combobox(self.rt, state="readonly",
                                        values=("Без ремонта", "Косметический", "Евроремонт", "Дизайнерский"),
                                        textvariable=repairs, width=14, justify="center")
        repairs.trace('w', lambda *args, v=repairs: self.flat_callback('num_repairs', repairs_combobox))

        repairs_combobox.place(x=630, y=500)

    def set_floors(self):
        floor = StringVar()
        Label(self.rt, text="Этаж:", font=self.std_font).place(x=20, y=600)
        floor_combobox = ttk.Combobox(self.rt, state="readonly",
                                      values=("Первый", "Последний", "Ни первый, ни последний"),
                                      textvariable=floor, width=25, justify="center")
        floor.trace('w', lambda *args, v=floor: self.flat_callback('num_floor', floor_combobox))
        floor_combobox.place(x=100, y=600)

    def set_home_types(self):
        home_t = StringVar()
        Label(self.rt, text="Тип дома:", font=self.std_font).place(x=440, y=600)
        home_type_combobox = ttk.Combobox(self.rt, state="readonly",
                                          values=(
                                          "Блочный", "Панельный", "Монолитный", "Кирпичный", "Монолитно-кирпичный"),
                                          textvariable=home_t, width=23, justify="center")
        home_t.trace('w', lambda *args, v=home_t: self.flat_callback('home_type', home_type_combobox))
        home_type_combobox.place(x=560, y=600)

    def sale_type_guard_parking_penthouse(self):
        self.guard = IntVar()
        self.parking = IntVar()
        self.penthouse = IntVar()
        self.sale_t = IntVar()
        gua = ttk.Style()
        gua.configure('TCheckbutton', font=self.std_font)
        ttk.Checkbutton(self.rt, text='Новостройка', variable=self.sale_t, onvalue=2, offvalue=0).place(x=20, y=660)
        ttk.Checkbutton(self.rt, text='Пентхаус', variable=self.penthouse, onvalue=2, offvalue=0).place(x=200, y=660)
        ttk.Checkbutton(self.rt, text='Подземная парковка', variable=self.parking, onvalue=2, offvalue=0).place(x=440, y=660)
        ttk.Checkbutton(self.rt, text='Охрана', variable=self.guard, onvalue=2, offvalue=0).place(x=675, y=660)
        Label(self.rt, text="Цена, руб:", font=("Times New Roman", 14, "bold")).place(x=20, y=730)
        ttk.Button(self.rt, text='Результат', command=self.prediction_callback).place(x=675, y=730)

if __name__ == "__main__":
    #create first neural network window
    root = Tk()
    setNeuralParamWindow = SetParametersWindow(root)
    set_neural_commands = ['set_number_of_hidden_neurons','set_learning_rate','momentum','batch',
                'low_weight','high_weight','bias','max_iterations','error_limit','stop',
                'hidden_act_func','output_act_func','buttons']
    for command in set_neural_commands:
        getattr(setNeuralParamWindow, command)()
    without_train = setNeuralParamWindow.without_train
    root.mainloop()

    if not without_train:
        neural_net_param = \
        main(setNeuralParamWindow.hidden.get(),
             setNeuralParamWindow.l_rate.get(),
             setNeuralParamWindow.moment.get(),
             setNeuralParamWindow.batch.get(),
             setNeuralParamWindow.l_weight.get(),
             setNeuralParamWindow.h_weight.get(),
             setNeuralParamWindow.bias.get(),
             setNeuralParamWindow.max_it.get(),
             setNeuralParamWindow.err_limit.get(),
             setNeuralParamWindow.stop_num.get(),
             setNeuralParamWindow.var.get(),
             setNeuralParamWindow.var2.get())

        #create second output neural net window
        root2 = Tk()
        getNeuralNetworkResults = PrintNetworkResultWindow(root2, *neural_net_param)
        getNeuralNetworkResults.output_info()
        getNeuralNetworkResults.evaluate_button()
        root2.mainloop()

    # create third output flat window
    root3 = Tk()
    flatWindow = GetFlatWindowResults(root3)
    set_flat_commands = ['set_area', 'set_distance_to_metro', 'set_squere',
                         'set_rooms_balcons_bathrooms','set_windows', 'set_seilings', 'set_repairs',
                         'set_floors', 'set_home_types', 'sale_type_guard_parking_penthouse']
    for command in set_flat_commands:
        getattr(flatWindow, command)()
    root3.mainloop()






