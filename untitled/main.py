from neuralnet import NeuralNet
from cost_activation_functions import relative_squared_error
from tools import neural_network_test, hidden_activation, output_activation
import xlrd, xlwt

n_inputs = 17

def dividing_samples_on_inputs_targets(num_samples, url, inp, out):

    rb = xlrd.open_workbook(url, formatting_info=True)
    sheet = rb.sheet_by_index(0)
    for i in range(num_samples):
        inp.append([])
        out.append([])
        for j in range(n_inputs):
            inp[i].append(sheet.cell(i, j).value)
        out[i].append(sheet.cell(i, n_inputs).value)


def main(n_hiddens, learning_rate, momentum_factor, batch_size, weights_low, weights_high, initial_bias_value, max_iterations, error_limit, stop, number_hidden_function, number_output_function):

    n_outputs = 1
    num_train_samples = 21092
    num_valid_samples = 4526
    num_test_samples = 4517

    training_data, training_targets, validation_data, validation_targets, test_data, test_targets = [], [], [], [], [], []

    # Read of parameter's excel file
    dividing_samples_on_inputs_targets(num_train_samples,'Training.xls', training_data, training_targets)
    dividing_samples_on_inputs_targets(num_valid_samples, 'Validation.xls', validation_data, validation_targets)
    dividing_samples_on_inputs_targets(num_test_samples, 'Test.xls', test_data, test_targets)

    cost_function = relative_squared_error

    # Initialize the neural network[set random weights]
    network = NeuralNet(n_inputs, n_hiddens, n_outputs, weights_low, weights_high, initial_bias_value, number_hidden_function, number_output_function)

    # Train the network using backpropagation
    valid_error, train_error, epoch, valid_error_array, train_error_array, epoch_array, valid_out, train_out, r_squere_train, r_squere_valid = \
    NeuralNet.backpropagation(network, training_data, validation_data, training_targets, validation_targets, cost_function,
                              learning_rate, momentum_factor, batch_size, num_train_samples, error_limit, max_iterations, early_stop=(stop))

    # Test network
    test_error, test_out, r_squere_test = neural_network_test(network, test_data, test_targets, cost_function)

    # Initialize a workbook
    book = xlwt.Workbook()

    # Add a sheet to the workbook
    sheet1 = book.add_sheet("Results")

    # Tittles and values for excel
    titles_row = ["Количество тренировочных примеров", "Количество валидационных примеров","Количество тестовых примеров",
              "Функция ошибок", "Скрытая функция активации", "Выходная функция активации", "Количество входных нейронов",
              "Количество скрытых нейронов", "Минимальный начальный вес", "Максимальный начальный вес", "Начальное смещение",
              "Количество весов", "Момент", "Скорость обучения", "Max iterations", "Batch size", "Ограничение ошибки",
              "Итоговое количество эпох", "Ранняя остановка", "Итоговая ошибка тренировки", "Итоговая ошибка валидации",
              "Итоговая тестовая ошибка","Тренировочный R-квадрат", " Валидационный R-квадрат", "Тестовый R-квадрат",
              "№ скрытой функции активации", "№ выходной функции активации"]

    titles_column = ["Предсказанный тренировочный набор", "Предсказанный валидационный набор",
                     "Предсказанный тестовый набор", "№ эпохи", "Валидационная ошибка",
                     "Тренировочная ошибка", "Веса сети"]

    values = [num_train_samples, num_valid_samples, num_test_samples,"Relative squered error", hidden_activation[number_hidden_function][0],
              output_activation[number_output_function][0], n_inputs, n_hiddens, weights_low, weights_high, initial_bias_value,
              network.n_weights, momentum_factor, learning_rate, max_iterations, batch_size, error_limit, epoch, stop, train_error,
              valid_error, test_error, r_squere_train, r_squere_valid, r_squere_test, number_hidden_function, number_output_function]

    #Record to the excel file
    for i in range(len(titles_row)):
         sheet1.write(i, 0, titles_row[i])
         sheet1.write(i, 1, values[i])

    for i in range(2, 9):
        sheet1.write(0, i, titles_column[i-2])

    for i in range(1, num_train_samples + 1):
        sheet1.write(i, 2, train_out[i-1][0])

    for i in range(1, num_valid_samples + 1):
        sheet1.write(i, 3, valid_out[i-1][0])

    for i in range(1, num_test_samples + 1):
        sheet1.write(i, 4, test_out[i - 1][0])

    for i in range(1, len(valid_error_array) + 1):
        sheet1.write(i, 6, valid_error_array[i - 1])
        sheet1.write(i, 5, epoch_array[i - 1])
        sheet1.write(i, 7, train_error_array[i - 1])

    q, w = n_inputs + n_outputs, n_hiddens
    s = 1
    for k, weight_layer in enumerate(network.weights):
        for i in range(q):
            for j in range(w):
                sheet1.write(s, 8, weight_layer[i][j])
                s += 1
        w = 1
        q = n_hiddens + n_outputs

    book.save("Results.xls")

    neural_net_param = [epoch, train_error, valid_error, test_error, r_squere_train, r_squere_valid, r_squere_test]
    return neural_net_param