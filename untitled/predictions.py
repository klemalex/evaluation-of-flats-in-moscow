import xlrd
from numpy import dot, array
from tools import hidden_activation, output_activation
import math


def prediction(metro, rooms, area, pent, g_squere, l_squere, k_squere, floor, house_type, parking,
               remont, window, ciling, bath, balcon, guard, type_sel, without_train):

    if (without_train):
        url1 = "Best_results.xls"
    else:
        url1 = "Results.xls"

    #Take values from excel
    rb = xlrd.open_workbook(url1,formatting_info=True)
    sheet = rb.sheet_by_index(0)
    n_inputs = sheet.cell(6, 1).value
    n_hiddens = sheet.cell(7, 1).value
    number_hidden_function = sheet.cell(25, 1).value
    number_output_function = sheet.cell(26, 1).value

    n_outputs = 1
    weights = []

    for i in range(int(n_hiddens)*(int(n_inputs)+int(n_outputs))+int(n_hiddens)+int(n_outputs)):
        weights.append([])
        weights[i] = sheet.cell(i + 1, 8).value

    #Reshape weigts array
    wei = array(weights)
    start, stop = 0, 0
    mas = []
    previous_shape = int(n_inputs) + 1
    n_neurons = int(n_hiddens)
    for i in range(2):
        stop += previous_shape * n_neurons
        mas.append(wei[start:stop].reshape(previous_shape, n_neurons))
        previous_shape = n_neurons + 1
        start = stop
        n_neurons = int(n_outputs)

    # Form a prediction set
    predict = [metro, rooms, area, pent, g_squere, l_squere, k_squere, floor, house_type, parking,
               remont, window, ciling, bath, balcon, guard, type_sel]

    # Use a forward propogation for predict
    for i, weight_layer in enumerate(mas):
        signal = dot(predict, weight_layer[1:, :]) + weight_layer[0:1, :]
        if i == 0:
            predict = hidden_activation[number_hidden_function][1](signal)
        else:
            predict = output_activation[number_output_function][1](signal)

    # Use a reverse normalization to get real value
    translated_predict = 14304253+2*13628541*math.log(predict/(1-predict))/2*math.pi

    return translated_predict