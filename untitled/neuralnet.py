from cost_activation_functions import *
from tools import dropout, add_bias, hidden_activation, output_activation
import collections
import random
import math

class NeuralNet:
    def __init__(self, n_inputs, n_hiddens, n_outputs, weights_low, weights_high, initial_bias_value, number_hidden_function, number_output_function):

        dict = {"layers": [(hidden_activation[number_hidden_function][1]), (output_activation[number_output_function][1])]}

        self.__dict__.update(dict)

        # Count the number of weights
        self.n_weights = (n_inputs + 1) * n_hiddens + (n_hiddens+n_outputs)

        # Initialize the network with random weights
        self.set_weights(self.generate_weights(weights_low, weights_high), n_hiddens, n_outputs, n_inputs)

        # Initialize the bias to 0.01
        for index in range(2):
            self.weights[index][:1, :] = initial_bias_value


    def generate_weights(self, weights_low, weights_high):
        return np.random.uniform(weights_low, weights_high, size=(self.n_weights,))

    # Divide 1 arrays on several for each neuron
    def set_weights(self, weight_list,n_hiddens,n_outputs,n_inputs ):

        start, stop = 0, 0
        self.weights = []
        previous_shape = n_inputs + 1
        n_neurons = n_hiddens

        for i in range(2):
            stop += previous_shape * n_neurons
            self.weights.append(weight_list[start:stop].reshape(previous_shape, n_neurons))
            
            previous_shape = n_neurons + 1
            start = stop
            n_neurons = n_outputs

    def backpropagation(network, training_data, valid_data, training_targets, valid_targets, cost_function,
                        learning_rate, momentum_factor, batch_size, num_train_samples, error_limit, max_iterations, early_stop=()):

        #Calculate derivatives
        momentum = collections.defaultdict(int)

        def calculate_dW(layer_index, dX):
            dW = -learning_rate * dX + momentum_factor * momentum[layer_index]
            momentum[layer_index] = dW
            return dW

        #Split data for batch
        batch_training_data = np.array_split(training_data, math.ceil(1.0 * num_train_samples / batch_size))
        batch_training_targets = np.array_split(training_targets, math.ceil(1.0 * num_train_samples / batch_size))

        batch_indices = list(range(len(batch_training_data)))

        valid_error = cost_function(network.update(valid_data), valid_targets)
        train_error = cost_function(network.update(training_data), training_targets)

        reversed_layer_indexes = range(2)[::-1]

        # Init errors and weights array
        train_error_array = []
        valid_error_array = []
        epoch_array = []
        best_weights = []
        min_valid_error, final_train_eror = 1, 1

        rise_error = 0 #Early stopping error
        epoch = 0

        input_layer_dropout = 0.0
        hidden_layer_dropout = 0.0

        while valid_error > error_limit and epoch < max_iterations:
            epoch += 1
            random.shuffle(batch_indices)

            for batch_index in batch_indices:
                batch_data = batch_training_data[batch_index]
                batch_targets = batch_training_targets[batch_index]

                # Calculate derivatives
                input_signals, derivatives = network.update(batch_data, trace=True)
                out = input_signals[-1]
                cost_derivative = cost_function(out, batch_targets, derivative=True).T
                delta = cost_derivative * derivatives[-1]

                #Correct weights
                for i in reversed_layer_indexes:

                    dropped = dropout(
                        input_signals[i],
                        hidden_layer_dropout if i > 0 else input_layer_dropout
                    )

                    # Calculate the weight change
                    dX = (np.dot(delta, add_bias(dropped)) / batch_size).T
                    dW = calculate_dW(i, dX)

                    if i != 0:
                        weight_delta = np.dot(network.weights[i][1:, :], delta)

                        # Calculate the delta for the next layer
                        delta = weight_delta * derivatives[i - 1]

                    # Update the weights
                    network.weights[i] += dW

            # Calculate errors for each epoch
            valid_error = cost_function(network.update(valid_data), valid_targets)
            train_error = cost_function(network.update(training_data), training_targets)

            # Perform early stop
            if min_valid_error > valid_error:
                best_weights = network.weights
                final_train_eror = train_error
                min_valid_error = valid_error
                rise_error = 0
            else:
                rise_error += 1
                if rise_error == early_stop:
                    print('[training] Early Stop (Error increased for more than ', rise_error, ' epochs)')
                    break
            # Insert errors and epochs to the list
            train_error_array.append(train_error)
            valid_error_array.append(valid_error)
            epoch_array.append(epoch)

            print("[training] Current training error:", train_error)
            print("[training] Current validation error:", valid_error)
            print("[training] Epoch:" , epoch)
            print("\n")

        network.weights = best_weights

        # Calculate R-squere and network out
        valid_out = network.update(valid_data)
        train_out = network.update(training_data)
        r_squere_train = r_squere(train_out, training_targets)
        r_squere_valid = r_squere(valid_out, valid_targets)

        print("[training] Train R-Squere: %.4g" % r_squere_train)
        print("[training] Valid R-Squere: %.4g" % r_squere_valid)
        print("[training]  Error bound (%.4g) with error %.4g." % (error_limit, valid_error))
        print("[training]  Trained for %d epochs." % epoch)

        return min_valid_error, final_train_eror, epoch, valid_error_array, train_error_array,\
               epoch_array, valid_out, train_out, r_squere_train, r_squere_valid

    def update(self, input_values, trace=False):

        #Forward propogation (calculate the network output)
        output = input_values

        # Derivatives of the act functions
        if trace:
            derivatives = []
            outputs = [output]

        for i, weight_layer in enumerate(self.weights):
            signal = np.dot(output, weight_layer[1:, :]) + weight_layer[0:1, :]
            output = self.layers[i](signal)

            if trace:
                outputs.append(output)

                #Derivatives for weight update
                derivatives.append(self.layers[i](signal, derivative=True).T)
        
        if trace:
            return outputs, derivatives
        
        return output

