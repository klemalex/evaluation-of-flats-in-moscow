import numpy as np

def sigmoid_function(signal, derivative=False):
    # Prevent overflow
    signal = np.clip(signal, -500, 500)

    signal = 1.0 / (1 + np.exp(-signal))

    if derivative:
        return np.multiply(signal, 1 - signal)
    else:
        return signal

def LReLU_function(signal, derivative=False, leakage = 0.01):

    if derivative:
        return np.clip(signal > 0, leakage, 1.0)
    else:
        output = np.copy(signal)
        output[output < 0] *= leakage
        return output

def tanh_function(signal, derivative=False):

    signal = np.tanh(signal)
    if derivative:
        return 1-np.power(signal, 2)
    else:
        return signal

def softplus_function(signal, derivative=False):

    if derivative:
        return np.exp(signal) / (1 + np.exp(signal))
    else:
        return np.log(1 + np.exp(signal))

def softsign_function(signal, derivative=False):

    if derivative:
        return 1. / (1 + np.abs(signal))**2
    else:
        return signal / (1 + np.abs(signal))

def relative_squared_error(outputs, targets, derivative=False):

    if derivative:
        return outputs - targets
    else:
        return np.sum(np.power(outputs - targets, 2))/np.sum(np.power(targets - np.mean(targets), 2))


def r_squere(outputs, targets, derivative=False):
    if derivative:
        return outputs - targets
    else:
        return 1 - (np.sum(np.power(outputs - targets, 2))/np.sum(np.power(targets-np.mean(targets), 2)))



